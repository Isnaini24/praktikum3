package com.IsnainiSaraswati_10191040.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;


public class ProfileActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    TextView txtNama, txtNim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtNama = findViewById(R.id.txtNama);
        txtNim = findViewById(R.id.txtNim);

        sharedPreferences = getSharedPreferences("mahasiswa", MODE_PRIVATE);
        txtNama.setText(sharedPreferences.getString("nama", null));
        txtNim.setText(sharedPreferences.getString("nim", null));
    }
}