package com.IsnainiSaraswati_10191040.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText edtName, edtNim;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtName = findViewById(R.id.edtName);
        edtNim = findViewById(R.id.edtNim);
        btnSubmit =findViewById(R.id.btnSubmit);

        sharedPreferences = getSharedPreferences("mahasiswa", MODE_PRIVATE);
        sharedPreferences.contains("nama");
        sharedPreferences.contains("nim");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("nama", edtName.getText().toString());
                editor.putString("nim", edtNim.getText().toString());

                editor.apply();
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });

    }
}